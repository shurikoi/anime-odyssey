function About(){
    return (
        <div className="textForAU">
            <div id="hello"><h2>Привіт!</h2></div>
            <br /><br />
            <div id="it"><p className="lead">Це — "Anime Odyssey", бо знайти аніме українською - справжня одісея.<br />Будь певний, що тут
                ти зможеш зручно дивитись аніме українською мовою.</p></div>
        </div>
    )
}

export default About