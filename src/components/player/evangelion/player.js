let episode=1;
let season=1;
const seasons=1;
const maxavailable=2;
const maxepisode=26;

let prev=document.getElementById("prev");
let next=document.getElementById("next");
let episodetext=document.getElementById("episode");
let player=document.getElementById("videoplayer");

player.src="season1/1.mp4";
player.load();

function prevepisode(){
  if (episode > 1){
  	episode--;
      episodetext.innerHTML=String(episode)+" серія "+String(season)+" сезон";
	player.src="season"+String(season)+"/"+String(episode)+".mp4";	
      player.load();
  }
}

function nextepisode(){
  if (episode < maxavailable){
    	episode++;
      episodetext.innerHTML=String(episode)+" серія "+String(season)+" сезон";
      player.src="season"+String(season)+"/"+String(episode)+".mp4";      
      player.load();
  }
}

prev.addEventListener("click",prevepisode);
next.addEventListener("click",nextepisode);