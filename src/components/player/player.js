let episode=1;
let season=1;
const seasons=1;
const maxavailable=3;
const anime_name="Evangelion"
let maxepisode=26;

if(maxavailable!==maxepisode){
  maxepisode=maxavailable;
}

let prev=document.getElementById("prev");
let next=document.getElementById("next");
let episodetext=document.getElementById("episode");
let player=document.getElementById("videoPlayer");
let episodelist=document.getElementById("episodeList");

player.src="anime/evangelion/season1/1.mp4";
player.load();

function setCookie(cname, cvalue, exdays) {
  const d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  let expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  let name = cname + "=";
  let decodedCookie = decodeURIComponent(document.cookie);
  let ca = decodedCookie.split(';');
  for(let i = 0; i <ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function checkCookie(namepisode) {
  let value = getCookie(namepisode);
  if (present != "") {
   player.currentTime=value;
  }
}


function prevepisode(){
  if (episode > 1){
  	episode--;
      episodetext.innerHTML=String(episode)+" серія "+String(season)+" сезон";
	episodelist.selectedIndex=episode-1;
	player.src="anime/evangelion/season"+String(season)+"/"+String(episode)+".mp4";	
      player.load();
      const value = getCookie(anime_name+"_"+String(episode));
      if (value != "") {
      player.currentTime=value;
      }
  }
}

function nextepisode(){
  if (episode < maxepisode){
    	episode++;
      episodetext.innerHTML=String(episode)+" серія "+String(season)+" сезон";
	episodelist.selectedIndex=episode-1;
      player.src="anime/evangelion/season"+String(season)+"/"+String(episode)+".mp4";      
      player.load();
      const value = getCookie(anime_name+"_"+String(episode));
      if (value != "") {
      player.currentTime=value;
      }      
  }
}

function episodelistselect(){
  episode=episodelist.selectedIndex+1;
  episodetext.innerHTML=String(episode)+" серія "+String(season)+" сезон";
  player.src="anime/evangelion/season"+String(season)+"/"+String(episode)+".mp4";      
  player.load();
}

function addcookie(){
  setCookie(anime_name+"_"+String(episode), player.currentTime, 14);
}

function findcookie(){
  let cookies= `; ${document.cookie}`.split("; Evangelion_");
  cookies.shift();
  //console.log(cookies);

  for(const el of cookies) {
    
    if (el.split("=")[0] == episode){
      //console.log(el, el.split("=")[0], el.split("=")[1]);
       return el.split("=")[1];
    }
}
}

function settime(){
  const value = findcookie();
  console.log(value);
  if (value==="Number") {player.currentTime=value};      
}

prev.addEventListener("click",prevepisode);
next.addEventListener("click",nextepisode);
episodelist.addEventListener("change",episodelistselect);
player.addEventListener("pause",addcookie);
player.addEventListener("playing",settime);
player.addEventListener("seeked",addcookie);