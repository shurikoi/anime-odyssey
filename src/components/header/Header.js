import "./Header.css"
import { Link, NavLink } from "react-router-dom"
import { useState } from "react"
import search from './search.svg'

function Header() {
    let [animes, setAnimes] = useState([])

    function Search(e) {
        let animeList = []
        let word = e.target.value.toLowerCase()
    
        let cards = require("../../cards.json").map(el => {
            return {
                name: el.name.split('\n').join(''),
                file_name: el.file_name,
                img_src: el.img_src,
		    genres: el.genres,
                to: el.to
            }
        })
    
        console.log(word.length)

        if (word.length > 0)
            cards.forEach((el, index) => {
                for (let i = 0; i < el.name.length; i++) {
                    if (word == el.name.toLowerCase().slice(i, i + word.length))
                        animeList.push(el)
                }
            })

        if(animeList.length >= 1)
            e.target.classList.add("finded")
        else
            e.target.classList.remove("finded")

        setAnimes(new Set(animeList))

    }

    return (
        <div className="header">
            <div className="navigation">
                <NavLink to="/" className="logo">
                    ANIME
                    <br />
                    ODYSSEY
                </NavLink>
                <ul className="nav">
                    {/* <li className="anime"><NavLink className="nav-item" to="anime" end>Анiме</NavLink></li> */}
                    <li className="about"><NavLink className="nav-item" to="about">Про нас</NavLink></li>
                </ul>
                <div className="search-wrapper">
                    <div className="search-container">
                        <img src={search} alt="" />
                        <input onInput={Search} className="search" placeholder = "Пошук"/>
                    </div>
                    <div className="result-wrapper">
                        {
                                [...animes].map((el, index) => {
                                    return (
                                        <Link to={"anime/" + el.to} className="result-item" key={index}>
                                            <img src={el.img_src}className="result-img"></img>
                                            <div className="result-name-wrapper">
							      <div className="result-name">{el.name}</div>
							      <div className="genres-wrapper">{
								    el.genres.map((el, index) => {
									return <span className="result-genre">{el}</span>
								    })
								   }
							   	</div>
							  </div>
                                        </Link>
                                    )
                                })
                        }
                    </div>
                </div>
            </div>
            <a href="https://savelife.in.ua/en" className="helping"><span className="helping-text"><i>Допомогти Україні і Українській Армії</i></span></a>
        </div>
    )
}

export default Header