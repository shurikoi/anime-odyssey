import { Link } from "react-router-dom"
import "./Anime.css"

let cards = require("../../cards.json")

function Anime() {
    return (
        <div className="container">
            {cards.map((el, index) => {
                return (
                    <div className="card" key={index}>
                        <Link className="card-img-wrapper" to={"anime/" + el.to}>
                            <img src={el.img_src} alt="img" className="card-img" />
                            <div className="card-play-button"></div>
                        </Link>
                            <div className="card-name">
                                {el.name}
                            </div>
                    </div>
                )
            })}
        </div>
    )
}

export default Anime