import { useParams } from "react-router-dom"
import "./ShowAnime.css"
let animes = require("./animes.json")

function ShowAnime(){
    let param = useParams().anime
    return(
        animes.map((el, index) => {
            if (param === el.anime){
                return ( 
                    <div key={index} className="main-anime">
                        <style>
                            {`body{\n\tbackground-image: url(${el.body_img});\n\tbackground-attachment: fixed;\n\tbackground-size: cover;\n\tbackground-repeat: no-repeat;\n}`}
                        </style>
                        <div className="title">{el.title}</div>
                        <div className="yas">
                            <div className="year">{el.year} | </div>
                            <div className="age">{el.age}</div>
                            <div className="seasons"> | {el.seasons}</div>
                        </div>
                        <div className="description">{el.description}</div>
                    </div>
                )
            }
        })
    )
}

export default ShowAnime