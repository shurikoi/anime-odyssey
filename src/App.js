import {Routes, Route} from "react-router-dom"
import Anime from "./components/anime/Anime"
import About from "./components/about/About"
import Layout from "./components/layout/Layout"
import ShowAnime from "./components/animes/ShowAnime";
import './App.css';

function App() {
  return (
    <Routes>
      <Route path="/" element={<Layout />}>
        <Route index element={<Anime />} />
        <Route path="anime/:anime" element={<ShowAnime />}></Route>
        <Route path="about" element={<About />}></Route>
      </Route>
          {/* <Route path="evangelion" element={<Evangelion />}></Route>
          <Route path="attack-on-titan" element={<AttackOnTitan /> }></Route>
          <Route path="jojo" element={<JoJo />}></Route>
          <Route path="demon-slayer" element={<DemonSlayer />}></Route>
          <Route path="tokyo-ghoul" element={<TokyoGhoul />}></Route>
          <Route path="watamote" element={<WataMote />}></Route>
          <Route path="scums-wish" element={<ScumsWish />}></Route>
          <Route path="nichijou" element={<Nichijou />}></Route> */}
      {/* {cards.map((el, index) => {
            return (
              <Route key={index} path = {el.to} element = {el.file_name}></Route>
            )
          })} */}
    </Routes>
  );
}

export default App;
